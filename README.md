# Repository for [sitpune.dev](https://sitpune.dev "Engineering @ SIT")

Web portal for engineers & engineering at
[SIT Pune](https://sitpune.edu.in "Symbiosis Institute of Technology, Pune").
